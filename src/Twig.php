<?php

namespace mattanger\Slim;
use Psr\Http\Message\ResponseInterface;

class Twig extends \Slim\Views\Twig
{
    private $theme;
    private $layout;
    private $app;

    public function __construct($path, array $settings = [])
    {
        $this->theme = 'default';
        $this->layout = 'default';
        parent::__construct($path, $settings);
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    public function setLayout($layout) {
        $this->layout = $layout;
    }

    public function setApp($app) {
        $this->app = $app;
    }

    public function render(ResponseInterface $response, $template, $data = [])
    {
        $data['layout'] = $this->layout;
        $data['theme'] = $this->theme;
        $data['app'] = $this->app;
        return parent::render($response, $template, $data);
    }

}