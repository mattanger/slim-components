<?php
/**
 *
 */
namespace mattanger\Slim\Controller;
use Interop\Container\ContainerInterface;
use mattanger\Slim\Session;
use Psr\Log\LoggerInterface;
use Slim\Flash\Messages;
use Slim\Interfaces\RouterInterface;
use Slim\Http\Response;
use Slim\Http\Request;
use Slim\Views\Twig;
/**
 * Class Controller
 * @package mattanger\Slim\Controller
 */
abstract class Controller
{
    protected $container;
    protected $view;
    protected $flash;
    protected $logger;
    protected $session;
    protected $router;

    /**
     * Controller constructor.
     * @param RouterInterface $router
     * @param Twig $view
     * @param Messages $flash
     * @param LoggerInterface $logger
     */
    public function __construct(RouterInterface $router, Twig $view, Messages $flash, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->view = $view;
        $this->logger = $logger;
        $this->flash = $flash;
        $this->session = new Session();
    }

    abstract function __invoke(Request $request, Response $response, $args);

    /**
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        return $this->container->get($name);
    }

    public function __isset($name)
    {
        return $this->container->has($name);
    }

    public function getContainer() {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container) {
        $this->container = $container;
    }
}



