<?php
/**
 * Created by IntelliJ IDEA.
 * User: anger
 * Date: 4/4/17
 * Time: 7:50 AM
 */

namespace mattanger\Slim\Action;
use mattanger\Slim\Session;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class ApiAction
{
    protected $logger;
    protected $session;
    /**
     * ApiAction constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->session = new Session();
    }

    abstract function __invoke(Request $request, Response $response, $args);

    protected function Error($message, $exception = null) {
        $error = [
            'error' => [
                'message' => $message
            ]
        ];
        return $error;
    }
}