<?php
namespace mattanger\Slim\Action;
use mattanger\Slim\Session;
use Psr\Log\LoggerInterface;
use Slim\Flash\Messages;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

abstract class Action
{
    protected $view;
    protected $flash;
    protected $logger;
    protected $session;

    /**
     * Action constructor.
     * @param Twig $view
     * @param Messages $flash
     * @param LoggerInterface $logger
     */
    public function __construct(Twig $view, Messages $flash, LoggerInterface $logger)
    {
        $this->view = $view;
        $this->logger = $logger;
        $this->flash = $flash;
        $this->session = new Session();
    }


    abstract function __invoke(Request $request, Response $response, $args);
}