<?php
namespace mattanger\Slim;

/**
 * Class Session
 * @package mattanger
 */
class Session
{

    /**
     * @param $key
     * @return null
     */
    public function get($key)
    {
        if (array_key_exists($key, $_SESSION)) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

    /**
     * @param $key
     * @param $val
     */
    public function set($key, $val)
    {
        $_SESSION[$key] = $val;
    }

    /**
     * @param $key
     * @return null
     */
    function __get($key)
    {
        return $this->get($key);
    }

    /**
     * @param $key
     * @param $val
     */
    function __set($key, $val)
    {
        $this->set($key, $val);
    }

    /**
     * @param $key
     * @return bool
     */
    function __isset($key)
    {
        return isset($_SESSION[$key]);
    }

    /**
     *
     */
    public function destroy() {
        if (session_status() == PHP_SESSION_ACTIVE) session_destroy();
    }

    public function invalidate() {
        $this->destroy();
    }

}